<?php
/**
 * @file
 * Three tab template file
 *
 * $drupal_render_children is defined by ds_forms.
 * If ds_forms is not enabled, this variable will not be empty.
 *
 * $previous and $next variables provide the html for previous and next forms
 * when ds_forms is enabled.
 */
?>
<?php $isform = NULL; if (!empty($drupal_render_children)): $isform = 'form'; endif; ?>
<div class="<?php print $classes;?>">

  <?php if (isset($title_suffix['contextual_links'])): print render($title_suffix['contextual_links']); endif; ?>
  
  <div id="tabbed-content-header" class="tabbed header">
    <?php if ($header):  print $header; endif; ?>
  </div>

  <div id="tabbed-container">

    <?php print ds_tabbed_layouts_tabs(4); ?>

    <div id="tabbed-content">	
      <div id="tabbed-content-1" class="tabbed tabbed-4-1">
        <?php if ($tabbed_4_1):  print $tabbed_4_1; ?>
          <?php if (!empty($isform)): print ds_tabbed_layouts_next(2); endif; ?>
        <?php endif; ?>
      </div>
      <?php $i = 2; $k = 4; while ($i <= ($k - 1)): $region = 'tabbed_' . $k . '_' . $i; print ds_tabbed_layouts_middle_content($tabs = $k, $tab = $i, $$region, $isform); $i++; endwhile; ?>
      <div id="tabbed-content-4" class="tabbed tabbed-4-4">
        <?php if ($tabbed_4_4): print $tabbed_4_4; ?>
        <?php endif; ?>
        <?php if (!empty($isform)): print ds_tabbed_layouts_previous(3); endif; ?>
        <?php if (!empty($isform)): print $drupal_render_children; endif; ?>
      </div>
    </div>
  </div>

  <div id="accordion-container">
    <?php $i = 1; $k = 4; while ($i <= $k): $region = 'tabbed_' . $k . '_' . $i; print ds_tabbed_layouts_accordion($i, $$region); $i++; endwhile; ?>
    <?php if (!empty($isform)): print $drupal_render_children; endif; ?>
  </div>

  <div id="tabbed-content-footer" class="tabbed footer">
    <?php if ($footer):  print $footer; endif; ?>
  </div>
</div>
