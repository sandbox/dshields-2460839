<?php
/**
 * @file
 * Two tab template file
 *
 * $drupal_render_children is defined by ds_forms.
 * If ds_forms is not enabled, this variable will not be empty.
 *
 * $previous and $next variables provide the html for previous and next forms
 * when ds_forms is enabled.
 */
?>

<div class="<?php print $classes;?>">

  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>
  
  <div id="tabbed-content-header" class="tabbed header">
    <?php if ($header):  print $header; ?>
    <?php endif; ?>
  </div>

  <div id="tabbed-container">

    <?php print ds_tabbed_layouts_tabs(2); ?>

    <div id="tabbed-content">	
      <div id="tabbed-content-1" class="tabbed tabbed-2-2col-1">
        <div class="col1">
          <?php if ($tabbed_2_2col_1_1):  print $tabbed_2_2col_1_1; endif; ?>
        </div>
        <div class="col2">
          <?php if ($tabbed_2_2col_1_2):  print $tabbed_2_2col_1_2; endif; ?>
        </div>
        <?php print ds_tabbed_layouts_next(2); ?>
      </div>
      <div id="tabbed-content-2" class="tabbed tabbed-2-2col-1">
        <div class="col1">

          <?php if ($tabbed_2_2col_2_1): print $tabbed_2_2col_2_1; endif; ?>
        </div>
        <div>
          <?php if ($tabbed_2_2col_2_2): print $tabbed_2_2col_2_2; endif; ?>
        </div>
        <?php print ds_tabbed_layouts_previous(1); ?>
        <?php if (!empty($drupal_render_children)): print $drupal_render_children; endif; ?>
      </div>
    </div>
  </div>

  <div id="accordion-container">
    <?php print ds_tabbed_layouts_accordion(1, $tabbed_2_2col_2_1); ?>
    <?php print ds_tabbed_layouts_accordion(2, $tabbed_2_2col_2_2); ?>
    <?php if (!empty($drupal_render_children)): print $drupal_render_children; endif; ?>
  </div>

  <div id="tabbed-content-footer" class="tabbed footer">
    <?php if ($footer):  print $footer; ?>
    <?php endif; ?>
  </div>
</div>
