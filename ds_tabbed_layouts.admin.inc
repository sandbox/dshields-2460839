<?php
/**
 * @file
 * Display Suite Tabbed Layouts admin.inc file.
 *
 * This file defines and validates the form for the admin page and makes overr
 */

/**
 * Form definition for the Display Suite Tabbed Layouts administration page.
 */
function ds_tabbed_layouts_admin_form() {
  $form = array();
  $form['ds_tabbed_layouts_breakpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Tab/Accordion Breakpoint'),
    '#default_value' => variable_get('ds_tabbed_layouts_breakpoint', '769'),
    '#size' => 10,
    '#maxlength' => 7,
    '#description' => t("Set the page width value at which the tabs should be rendered as an accordion for smaller screens."),
    '#required' => TRUE,
  );
  $form['ds_tabbed_layouts_tab_labels_container'] = array(
    '#type' => 'fieldset',
    '#attributes' => array(
      'id' => 'ds-tabbed-layouts-tab-labels-container',
    ),
    '#prefix' => '<div class="tab-default-labels-description description">' . t('Here, you can set the site-wide default labels for the DS Tabbed Layouts.<br />You can override these values for any node using this layout in the <em>Display Suite Tabbed Layout settings</em> vertical tab on the node-edit page.') . '</div>',
  );
  $i = 1;
  while ($i < 9) {
    $form['ds_tabbed_layouts_tab_labels_container']['ds_tabbed_layouts_tab' . $i . '_label'] = array(
      '#type' => 'textfield',
      '#title' => t('Tab') . ' ' . $i . ' ' . t('Label'),
      '#default_value' => variable_get('ds_tabbed_layouts_tab' . $i . '_label', t('Tab @i', array('@i' => $i))),
      '#size' => 30,
      '#maxlength' => 30,
      '#required' => TRUE,
    );
    $i++;
  }

  return system_settings_form($form);
}

/**
 * Validation for the administration form.
 */
function ds_tabbed_layouts_admin_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  if (!is_numeric($values['ds_tabbed_layouts_breakpoint']) || $values['ds_tabbed_layouts_breakpoint'] < 0) {
    form_set_error('ds_tabbed_layouts_breakpoint', t('Your Tab/Accordion Breakpoint value must be a number between 1 and something reasonable.'));
  }
}
