/**
 * @file
 * Display Suite Tabbed Layouts next/previous buttons file.
 *
 * This script adds functionality to the next and previouis tab buttons for
 * instances when the layout is being used on a form. It also scrolls the
 * browser to the top of the page when moving from one tab to another when
 * navigating via the next/previous buttons.
 */
(function($, Drupal)
{
  Drupal.behaviors.nextButton = {
    attach: function (context, settings) {
      if ($('#tabbed-container').length) {
        $('.next, .previous').click(function(){
          $("html, body").animate({ scrollTop: 0 }, "slow");
          var tab = $(this).html().replace(/[^\d.]/g,'');
          $('input:radio[name=tab-group]').val([tab]);	
        });
      }
    }
  }
}(jQuery, Drupal));