/**
 * @file
 * Display Suite Tabbed Layouts container height file.
 *
 * This script sets the height of the container to the height of the tallest
 * div of all those that sit within the tabbed-content region.
 */
(function($, Drupal)
{
  Drupal.behaviors.containerHeight = {
    attach: function (context, settings) {
      var maxheight = 0;
      $('div.tabbed').each(function() {
        var height = $(this).height();
        var margin = parseInt($(this).css('margin-top').slice(0,-2));
        height = height + margin;
        if (height > maxheight) {
          maxheight = height;
        }
      });
      $('div#tabbed-content').height(maxheight);
    }
  }
}(jQuery, Drupal));
