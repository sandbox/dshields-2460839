CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
Similar to DS Extra Layouts, this module provides a number of new layouts for Display Suite, but all of these layouts are CSS3-based tabbed layouts that transform into accordion regions at a certain breakpoint.

 * For a full description of the module, visit the project page:
  https://www.drupal.org/sandbox/dshields/2460839
 * To submit bug reports and feature suggestions, or to track changes:
	https://www.drupal.org/project/issues/2460839
	
REQUIREMENTS
------------
This module requires the following modules:
 * Display Suite (https://drupal.org/project/ds)
 * Chaos Tools (https://drupal.org/project/ctools)
	
RECOMMENDED MODULES
-------------------
 * DS Extra Layouts (https://www.drupal.org/project/ds_extra_layouts):
 This module adds additional layouts for Display Suite. The extra layouts are more reusable than the custom layouts because it is packaged as a module. This does not completely eliminate the need for a custom DS layouts, however, it does reduce the it.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
	https://drupal.org/documentation/install/modules-themes/modules-7
	for further information.

CONFIGURATION
-------------
 * After installation, you'll find configuration for the module at http://example.com/admin/structure/ds/ds_tabbed_layouts.

FAQ
---
Q: Can I override the .tpl.php files?
A: Yep. Just copy the .tpl.php file into your theme's templates directory and make any alterations you like.

MAINTAINERS
-----------
Current maintainers:
 * Dylan Shields (dshields) - https://drupal.org/user/1539066

This project has been sponsored by:
 * OPENCONCEPT CONSULTING INC.
	Visit https://www.openconcept.ca for more information.